package view;
	import java.awt.*;
	import javax.swing.*;

	/* The Window class creates the Graphical User Interface */
	public class Window extends JFrame {

				//create the GUI elements
	    		public JTextField t0=makeTextFields();
	    		public JTextField t1=makeTextFields();
	    		public JTextField t2=makeTextFields();
	    		public JTextField t3=makeTextFields();
	    		
	    		public static JTextArea area=new JTextArea(12, 78);
	    		 
		        JPanel panel=makePrincipalPanels("Servers Panel");
		        JPanel panelOut=makePrincipalPanels("Area of events Panel");
		        JScrollPane pa = new JScrollPane(area);
	    		SpringLayout springLayout = new SpringLayout();
	    		
	    		JFrame frame=makeFrame("Queue analysing system");
				    
			    JLabel firstServerLabel=makeLabels("First Server Entry -->");
			    JLabel secondServerLabel=makeLabels("Second Server Entry -->");
			    JLabel thirdServerLabel=makeLabels("Third Server Entry -->");
			    JLabel arrowLabel=makeLabels("---->  Exiting Servers");
			    JLabel timeLabel=makeLabels("Global Time:");
			    
			   /* Constructor to setup the GUI
	    		*/
			   public Window() {        
			        frame.add(panel);
			        frame.add(panelOut);
			        panel.add(timeLabel);
			        panel.add(t3);
			        panel.add(firstServerLabel);
			        panel.add(t0); 
			        panel.add(secondServerLabel);
			        panel.add(t1);
			        panel.add(thirdServerLabel);
			        panel.add(t2);
			        panel.add(arrowLabel);
			        panel.setLayout(springLayout);
			        springLayout.putConstraint(SpringLayout.WEST, t0, 165, SpringLayout.WEST, firstServerLabel);
			        springLayout.putConstraint(SpringLayout.WEST, arrowLabel, 310, SpringLayout.WEST, t0);
			        springLayout.putConstraint(SpringLayout.SOUTH, t1, 40, SpringLayout.SOUTH, t0);
			        springLayout.putConstraint(SpringLayout.WEST, t1, 165, SpringLayout.WEST, secondServerLabel);
			        springLayout.putConstraint(SpringLayout.SOUTH, secondServerLabel, 43, SpringLayout.SOUTH, firstServerLabel);
			        springLayout.putConstraint(SpringLayout.WEST, t1, 165, SpringLayout.WEST, secondServerLabel);
			        springLayout.putConstraint(SpringLayout.SOUTH, thirdServerLabel, 43, SpringLayout.SOUTH, secondServerLabel);
			        springLayout.putConstraint(SpringLayout.WEST, t2, 165, SpringLayout.WEST, thirdServerLabel);
			        springLayout.putConstraint(SpringLayout.SOUTH, t2, 40, SpringLayout.SOUTH, t1);
			        springLayout.putConstraint(SpringLayout.SOUTH, timeLabel, 40, SpringLayout.SOUTH, thirdServerLabel);
			        springLayout.putConstraint(SpringLayout.WEST, t3, 165, SpringLayout.WEST, timeLabel); 
			        springLayout.putConstraint(SpringLayout.SOUTH, t3, 40, SpringLayout.SOUTH, t2); 
			        pa.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
			        panelOut.add(pa);			        
			        frame.setVisible(true);
			        }
			     
			   //method to create a frame
			   public JFrame makeFrame(String frame_text)
			   {
				  //FRAME
				    JFrame frame = new JFrame(frame_text);
			        frame.setPreferredSize(new Dimension(900, 500));
			        frame.setLayout(new GridLayout(2, 1));
			        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			        frame.pack();
			        frame.setLocationRelativeTo(null);
			        frame.setVisible(true);
			        return frame;
			   }
			 	   
			   //method to create a label
			   public JLabel makeLabels(String label_text)
			   {
				 //LABELS
			        JLabel label = new JLabel();
			        label.setText(label_text); 
			        label.setFont(new java.awt.Font("Times New Roman", 1, 15));
			        return label;
			   }
			   
			   //method to create a text field
			   public JTextField makeTextFields()
			   {
				 //TEXT FIELDS
			        JTextField t= new JTextField();
			        t.setPreferredSize( new Dimension( 300, 30 ));
			        return t;
			   }
			   	  
			   //method to create a principal panel
			   public JPanel makePrincipalPanels(String panel_name)
			   {
				   //PRINCIPAL PANELS panel and panelOut
				   JPanel panel=new JPanel();
				   panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), panel_name));
			       panel.setBounds(500, 400, 500, 1020);   
				   return panel;
			   }
			   
			     	    
			   //Method to set up the text of the first server into t0
			   public void setServerS1(String s) {
			        t0.setText(s+ " ");
			    }
			   
			   //Method to set up the text of the second server into t1
			   public void setServerS2(String s) {
			        t1.setText(s+ " ");
			    }
			   
			   //Method to set up the text of the third server into t2
			   public void setServerS3(String s) {
			        t2.setText(s+ " ");
			    }
			   
			   //Method to set up the text of the globall timer into t3
			   public void setTheTime(String s) {
			        t3.setText(s+ " ");
			    }
			   
			   //Method to print the conclusions into the text area
			   public static void setResult(String s) {
				   area.append(s+"\n");
			    }
			  
}
