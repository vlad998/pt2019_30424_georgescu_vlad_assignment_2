package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import view.Window;

/*
 * SimulationManager class that generates clients with random arrivingTime, id and servicingTime.
 * Contains the simulation loop which calls the scheduler that dispatch/distribute the clients.
 * It also updates the UI by inserting clients' id into their corresponding text field according to their server and
 * displays relevant information about the processing into text area log of events
 * @see Client, Window, SortArrival
 */
public class SimulationManager extends Thread {
	public int timeLimit=65;   //time limit of the entire process
	public int maxServicingTime=8;  //maximum time for servicing
	public int minServicingTime=4;  //minimum time for servicing
	public int nrServers=3;  //nr of servers 
	public static int nrClients=25;   //total number of clients to be distributed to the servers
	
	private Scheduler scheduler;
	private List<Client> generatedClients=new ArrayList<Client>();  //array list of generated clients
	private Window frame;  
	
	//constructor to set up the SimulationManager
	public SimulationManager()
	{
		scheduler=new Scheduler(nrServers, nrClients);   //create a new scheduler with nrServers and nrCients
		frame= new Window();
		generateRandomClients();  //generate randomly the clients
	}
	
	/*Method that generates random clients with random id, random arriving times and random servicing times(in 4-8 sec interval)
	 * Then, the list with generated servers is sorted in ascending order according to the arriving times of the 
	 * generated clients. 
	 */
	private synchronized void generateRandomClients()
	{
		Random rand = new Random();
		int s, a, id=1;
		for(int i=0; i<nrClients*2; i++)
		{
			Client c;
			
			s=rand.nextInt(maxServicingTime-4)+minServicingTime;  //[4-8]seconds interval
			a=rand.nextInt(timeLimit-15)+2;  //[2-52]seconds interval
			
			c= new Client(a, s, id);
			generatedClients.add(c);
			id++;
		}
		Collections.sort(generatedClients, new SortArrival());
		
		}
	
	/*
	 * The method that generates the main thread with global clock on which the process is executed
	 */
	@Override
	public void run() 
	{
		int currentTime=1;  //set the global current time
		try {
			synchronized(generatedClients)  //synchronize the generated clients
			{
				
		while(currentTime<=timeLimit)   //As long as the time has not finished, the process is executed
		{
			//Traverse the generatedClients list using the it iterator
			Iterator<Client> it = generatedClients.iterator();
			 
			while(it.hasNext()) 
			{
				//If it happens that the arrival time of a client from the list is equal with the current time
				//then the client is sent to the queue and removed from the list
				if(it.next().getArrivalTime()==currentTime)
				{
					scheduler.dispatchClient(it.next());
					it.remove();
				}
			}
			
			//update frame characteristics
			frame.setServerS1(scheduler.servers.get(0).qu1);
			frame.setServerS2(scheduler.servers.get(1).qu1);
			frame.setServerS3(scheduler.servers.get(2).qu1);
			frame.setTheTime("Current time is: " + currentTime);  
			currentTime++;  //increment the globall time
			Thread.sleep(1000);  //put the thread to sleep for one second
			} 
		Window.setResult("Average waiting time of shop is: " + Server.average());
			}
			}
		catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	/*
	 * Program driver method that start thread
	 */
	public static void main(String[] args)
	{
		SimulationManager gen = new SimulationManager();
		Thread t = new Thread(gen);
		t.start();	
	}	
	
}
