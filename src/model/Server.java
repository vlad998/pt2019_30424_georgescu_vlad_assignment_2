package model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/*
 * Class Server is used to set up a queue
 * @see SimulationManager, Client
 * 
 */
public class Server extends Thread{
	
	 private BlockingQueue<Client> clients;  //blocking queue of clients
	 private int waitingPeriod=0;  //total waiting time of a queue
	 public String qu1 = "";     //string for printing the clients' ids in the UI
	 
	 public static int[] avg=new int[SimulationManager.nrClients];  //array of waiting time for each client, useful to compute the avg waiting time of the shop
	 public int i=0;
	 
	 /*
	  * constructor to set up a server
	  */
	 public Server()
	 {
		 this.clients=new ArrayBlockingQueue<Client>(SimulationManager.nrClients);
		 this.waitingPeriod=0;
	 }
	
	/*
	 * Method to add a client in the server. The waitinig period of the server is raised with the addedd client's service time
	 * @param a Client c which we want to add in the server
	 */
	public void addClient(Client c)
	{
		clients.add(c);
		waitingPeriod+=c.getServiceTime();
		avg[i++]+=waitingPeriod;
	}
	
	/*
	 * Run method to take the next client from the server
	 */
	 public void run() 
	 {
		 while(true) 
		 {
			 try {
				Client c=clients.take();  //take the client
				Thread.sleep(1000*c.getServiceTime());  //stop the thread for the service time of the client in seconds
				waitingPeriod-=c.getServiceTime();  //decrease the waiting period since the client was taken
			
				qu1 = qu1.substring(0, qu1.lastIndexOf(" "));  //take out the client id from the string
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		 }
	 }
	 
	 /*
	  * Method to compute the average waiting time
	  */
	 public static int average()
		{
			int i; 
			int sum=0;
			for(i=0; i<SimulationManager.nrClients; i++)
			{
				sum+=avg[i];
			}
			return (sum/SimulationManager.nrClients);
		}
	 
	 public BlockingQueue<Client> getClients()
	 {
		 return this.clients;
	 }
	 
	 public int getWaitingPeriod()
	 {
	 	return this.waitingPeriod;
	 }
	 	 
}
