package model;
import java.util.Comparator;

/*
 * SortArrival class that implements a comparator that helps in sorting the generatedClients by arrivalTime
 * @see SimulationManager
 */
public class SortArrival implements Comparator<Client> {

	public int compare(Client a, Client b) {
		return a.getArrivalTime()- b.getArrivalTime();
	}
	
}