package model;

import java.util.Collections;
import java.util.List;

import view.Window;

/*
 * StrategyTime class which inserts clients to servers according to minimum waitingTime of servers
 * @see Client, Server, SortWaiting
 */
	public class StrategyTime{
		
		/*Method to add client into the server with minimum waiting time
		*It gets the server with minimum waitingTime. It uses a class SortWaiting
		*which implements a Comparator. After the servers are sorted, insert the client into the
		*server with miminum waitingTime.
		*Add the client's id to the string of ids
		*And print the results on the text area
		*/
		public synchronized void addClient(List<Server> servers, Client c) {
			
			Server server= Collections.min(servers, new SortWaiting());
			
			int finishTime=server.getWaitingPeriod()+c.getArrivalTime()+c.getServiceTime();
			c.setFinishTime(finishTime);
			server.addClient(c);
			
			server.qu1= ' '+ Integer.toString(c.getId()) +server.qu1 ;  //add the client's id into the string
		
			Window.setResult("Client with id: " + c.getId() + "     arrived at time: " + c.getArrivalTime() + "      had servicing time: " + c.getServiceTime() + "      and leaved at time: " + c.getFinishTime());                                        
			}
	}

