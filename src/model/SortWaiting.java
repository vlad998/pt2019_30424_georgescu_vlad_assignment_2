package model;
import java.util.Comparator;

/*
 * SortWaiting class that implements a comparator that helps in sorting the servers by waitingPeriod
 * @see StrategyTime
 */
public class SortWaiting implements Comparator<Server> {

	public int compare(Server a, Server b) {
		return a.getWaitingPeriod() - b.getWaitingPeriod();
	}
	
}
