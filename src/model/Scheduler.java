package model;

import java.util.ArrayList;
import java.util.List;

/*
 * Scheduler class to send clients to Servers according to StrategyTime, 
 * which inserts clients to the Server with minimum waitingTime
 * @see Client, Server, StrategyTime
 */
public class Scheduler {
	public List<Server> servers= new ArrayList<Server>(); //array list of Servers
	private StrategyTime strategy= new StrategyTime();
	
	//Constructor to set up the Scheduler
	public Scheduler(int maxServers, int nrMaxClientsInServer)
	{
		for(int i=0; i<maxServers; i++)
		{
			Server s=new Server();
			servers.add(s);
			Thread t=new Thread(s);
			t.start();
		}
	}
	
	//Method to add the Client c into the server
	public void dispatchClient(Client c)
	{
		strategy.addClient(servers, c);
	}
	
	public List<Server> getServers()
	{
		return servers;
	}
	
}
