package model;


/*
 * Class.java is a Class is stands for describing the Client's attributes 
 * @see the rest of classes
 */
public class Client {
	
	private int arrivalTime;  //arriving time in queue
	private int finishTime;		//finishing time
	private int serviceTime;   //ervicing time
	private int id;    //random id of a client. Kind of identification number of each client
	  

/*
 * constructor for setting up a client
 * @param int arrivalTime is the arrival time of a client
 * @param int serviceTime is the service time of a client
 * @param int id is the id of a client 
 * 
 */
public Client(int arrivalTime, int serviceTime, int id)
{
	this.arrivalTime=arrivalTime;
	this.serviceTime=serviceTime;
	this.id=id;
}

public int getArrivalTime()
{
	return this.arrivalTime;
}

public int getServiceTime()
{
	return this.serviceTime;
}

public int getId()
{
	return this.id;
}

public int getFinishTime()
{
	return this.finishTime;
}

public void setFinishTime(int finishTime)
{
	this.finishTime=finishTime;
}


}